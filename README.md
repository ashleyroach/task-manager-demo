# Task Manager Demo

This is a example REST API application that demonstrates how to use several tools for automatically generated RAML and Markdown under Spring MVC 4.0.

The example uses DTOs and a Controller class to serve up the API.

The RAML and Markdown are generated during the Maven prepare-package phase (e.g., mvn clean package).

# Components

In this example, the components are added via the pom.xml.

* spring mvc -- 4.0.4.RELEASE
* [wsdoc](https://github.com/versly/wsdoc) -- 1.0-SNAPSHOT
* Jackson JSON Schema -- 2.4.1 
* [raml2md](https://github.com/aroach/raml2md)


# Anticipated developer workflow

```
1. developers do new work on the API.
2. build pipeline automatically generates RAML from code.
3. build pipeline merges RAML from prior day with new RAML. *
4. (optional) developers update RAML product of (3) with any additional useful information.
5a. build pipeline generates internal API docs from RAML and publishes to internal server.
5b. build pipeline performs automatic static analysis from RAML, checking for compliance against guidelines.
5c. build pipeline generates a report from deltas to RAML with notification (email, wiki, etc) of new changes in the API.
6. tech writers consume product of (4), (5a) and (5c).
7. internal users and API developers consume product of (5a) and (5b).
8. goto (1)
```

* for step (3) there is new tooling needed for this step.

# What elements in your source code are added to the RAML?

The API signatures as defined by Spring annotations (@RequestMapping,etc.) are the primary focal points for the outline of the documentation.  Javadocs that are created by the developer are also included.  Additionally, JSON Schema is determined via instrospection on the DTOs.