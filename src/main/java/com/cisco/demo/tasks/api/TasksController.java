package com.cisco.demo.tasks.api;

import com.cisco.demo.tasks.dto.TaskCollection;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.cisco.demo.tasks.dto.Task;

import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;

/**
 * Created by ashleyroach on 7/16/14.
 */

@RestController
@RequestMapping("/")
public class TasksController {


    /**
     * Get a task with the given id.  Id's are guids.
     * <pre>
     *     GET /tasks/{id} will return a specific task.
     * {
     *     "foo": "bar"
     * }
     * </pre>
     *
     * @param id This is an id field.  It is unique.
     * It also requires multiple lines to describe and has a colon right here :.
     *
     * Error codes: 400
     *
     */
    @RequestMapping(value = "/tasks/{id}",method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Task getTask(@PathVariable("id") Long id) {

        System.out.print("getTask");

        Task myTask = new Task();

        myTask.setName("Buy groceries");
        myTask.setState(Task.STATE.ACTIVE);

        return myTask;
    }

    /**
     * Get all of your tasks.
     *
     * <pre>
     *     GET /tasks will return a list of tasks.
     *
     * </pre>
     *
     * This method will return an array of Tasks that have been created.
     *
     */
    @RequestMapping(value = "/tasks",method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public TaskCollection getTasks() {

        System.out.print("getTasks");

        // Mock some data

        TaskCollection collection = new TaskCollection();
        ArrayList<Task> tasks = new ArrayList<Task>();

        for (int i=1; i<11; i++ ){
            Task t = new Task();
            //t.setURI();
            t.setName("Task" + i);
            if (i % 2 == 0 )
                t.setState(Task.STATE.ACTIVE);
            else
                t.setState(Task.STATE.DEFERRED);
            tasks.add(t);

        }

        collection.setTasks(tasks);

        return collection;
    }

    /**
     * Create a task.
     * <pre>
     * {
     *     "name" : "take out the groceries"
     * }
     * </pre>
     *
     * @param task formatted in JSON
     *
     */

    @RequestMapping(value="/tasks", method=RequestMethod.POST, headers = {"Content-type=application/json"})
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody String insertTask(@RequestBody Task task) {

        return "{\n" +
                "  status : \"OK\",\n" +
                "  errorMessage : \"\"\n" +
                "}";
    }

}
