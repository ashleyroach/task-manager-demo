package com.cisco.demo.tasks.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jsonSchema.JsonSchema;
import com.fasterxml.jackson.module.jsonSchema.factories.SchemaFactoryWrapper;

import java.net.URI;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Task {

    public enum STATE {
        COMPLETED,
        ACTIVE,
        DEFERRED
    }

    @JsonProperty(required = false)
    @JsonPropertyDescription("ID of the resource.")
    private String id = UUID.randomUUID().toString();

    @JsonProperty(required = false)
    @JsonPropertyDescription("Self identifying URL of the resource.")
    private URI url;

    public void setURI(String uri) {
        this.url = URI.create(uri);
    }

    /**
     * The name of the task
     */
    @JsonProperty(required = true)
    private String name;

    /**
     * The state of the task
     */
    @JsonProperty(required = true)
    private STATE state;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public STATE getState() {
        return state;
    }

    public void setState(STATE state) {
        this.state = state;
    }

}
