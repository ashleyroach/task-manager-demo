package com.cisco.demo.tasks.dto;

import java.util.ArrayList;

/**
 * Created by ashleyroach on 7/16/14.
 */
public class TaskCollection {

    private ArrayList<Task> _tasks = null;

    public ArrayList<Task> getTasks() {
        return _tasks;
    }

    public void setTasks(ArrayList<Task> _items) {
        this._tasks = _items;
    }

    public TaskCollection() {
    }
}
